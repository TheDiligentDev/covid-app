import React from 'react';
import { IonCard, IonCardContent } from '@ionic/react';
import { Line } from 'react-chartjs-2';

interface ChartProps {
    chartTitle: string;
    chartData: { date: string, total: number }[];
    chartColors: {
        borderColor: string,
        pointBorderColor: string,
        pointBackgroundColor: string,
        backgroundColor: string
    }
}

interface ChartState {
    chartData: {
        labels: string[],
        datasets: [{
            label: string,
            data: number[]
            borderColor: string,
            pointBorderColor: string,
            pointBackgroundColor: string,
            backgroundColor: string
        }]
    }
}

class Chart extends React.Component<ChartProps, ChartState> {
    constructor(props: ChartProps) {
        super(props);
        this.state = {
            chartData: {
                labels: [],
                datasets: [{
                    label: '',
                    data: [],
                    borderColor: '',
                    pointBorderColor: '',
                    pointBackgroundColor: '',
                    backgroundColor: ''
                }]
            }
        }
    }


    componentDidMount() {
        const dates = this.props.chartData.map(d => d.date).splice(1, 15).reverse();
        const totals = this.props.chartData.map(d => d.total).splice(1, 15).reverse();

        let componentState = { ...this.state.chartData };
        componentState.labels = dates;
        componentState.datasets = [
            {
                label: this.props.chartTitle,
                data: totals,
                borderColor: this.props.chartColors.borderColor,
                pointBorderColor: this.props.chartColors.pointBorderColor,
                pointBackgroundColor: this.props.chartColors.pointBackgroundColor,
                backgroundColor: this.props.chartColors.backgroundColor
            }
        ]

        this.setState({ chartData: componentState });
    }

    render() {
        return (
            <IonCard>
                <IonCardContent>
                    <Line
                        redraw
                        key={Math.random()}
                        data={this.state.chartData}
                        options={{
                            title: {
                                display: true,
                                text: this.props.chartTitle,
                                fontSize: 20
                            }
                        }}
                        height={300}
                    />
                </IonCardContent>
            </IonCard>
        );
    }
};

export default Chart;