import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, withIonLifeCycle, IonSpinner, IonRow, IonCol } from '@ionic/react';
import Chart from '../components/Chart';
import './Tab1.css';
import axios from "axios";
import moment from "moment";

interface chartData {
  date: string,
  total: number
}

interface chartColors {
  borderColor: string,
  pointBorderColor: string,
  pointBackgroundColor: string,
  backgroundColor: string
}

interface MyState {
  arrPositive: chartData[],
  positiveChartColors: chartColors,
  arrHospitalized: chartData[],
  hospitalizedChartColors: chartColors,
  arrInIcu: chartData[],
  inIcuChartColors: chartColors,
  arrOnVentilators: chartData[],
  onVentilatorsChartColors: chartColors,
  arrRecovered: chartData[],
  recoveredChartColors: chartColors,
  arrDeaths: chartData[],
  deathsChartColors: chartColors,
  isLoading: boolean
}

class Tab1 extends React.Component<{}, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      arrPositive: [],
      positiveChartColors: {
        borderColor: "#077187",
        pointBorderColor: "#0E1428",
        pointBackgroundColor: "#AFD6AC",
        backgroundColor: "#74A57F"
      },
      arrHospitalized: [],
      hospitalizedChartColors: {
        borderColor: "#251F47",
        pointBorderColor: "#260F26",
        pointBackgroundColor: "#858EAB",
        backgroundColor: "#858EAB"
      },
      arrInIcu: [],
      inIcuChartColors: {
        borderColor: "#190B28",
        pointBorderColor: "#190B28",
        pointBackgroundColor: "#E55381",
        backgroundColor: "#E55381"
      },
      arrOnVentilators: [],
      onVentilatorsChartColors: {
        borderColor: "#784F41",
        pointBorderColor: "#784F41",
        pointBackgroundColor: "#BBE5ED",
        backgroundColor: "#BBE5ED"
      },
      arrRecovered: [],
      recoveredChartColors: {
        borderColor: "#4E5E66",
        pointBorderColor: "#4E5E66",
        pointBackgroundColor: "#31E981",
        backgroundColor: "#31E981"
      },
      arrDeaths: [],
      deathsChartColors: {
        borderColor: "#E06D06",
        pointBorderColor: "#E06D06",
        pointBackgroundColor: "#402A2C",
        backgroundColor: "#402A2C"
      },
      isLoading: false
    }
  }

  async ionViewWillEnter() {
    this.setState({ isLoading: true });
    const { data } =
      await axios.get("https://covidtracking.com/api/us/daily");

    data.forEach((d: any) => {
      const date = moment(d.date, "YYYYMMDD").format("MM/DD");

      const {
        positive,
        hospitalizedCurrently,
        inIcuCurrently,
        onVentilatorCurrently,
        recovered,
        death
      } = d;



      this.setState({ arrPositive: [...this.state.arrPositive, { date, total: positive }] });
      this.setState({ arrHospitalized: [...this.state.arrHospitalized, { date, total: hospitalizedCurrently }] });
      this.setState({ arrInIcu: [...this.state.arrInIcu, { date, total: inIcuCurrently }] });
      this.setState({ arrOnVentilators: [...this.state.arrOnVentilators, { date, total: onVentilatorCurrently }] });
      this.setState({ arrRecovered: [...this.state.arrRecovered, { date, total: recovered }] });
      this.setState({ arrDeaths: [...this.state.arrDeaths, { date, total: death }] });
    });

    this.setState({ isLoading: false });

  }

  render() {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>United States</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">United States</IonTitle>
            </IonToolbar>
          </IonHeader>
          {this.state.arrPositive.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Positive" chartData={this.state.arrPositive} chartColors={this.state.positiveChartColors}></Chart>
            : ''
          }
          {this.state.arrHospitalized.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Hospitalized" chartData={this.state.arrHospitalized} chartColors={this.state.hospitalizedChartColors}></Chart>
            : ''
          }
          {this.state.arrInIcu.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="In ICU" chartData={this.state.arrInIcu} chartColors={this.state.inIcuChartColors}></Chart>
            : ''
          }
          {this.state.arrOnVentilators.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="On Ventilators" chartData={this.state.arrOnVentilators} chartColors={this.state.onVentilatorsChartColors}></Chart>
            : ''
          }
          {this.state.arrRecovered.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Recovered" chartData={this.state.arrRecovered} chartColors={this.state.recoveredChartColors}></Chart>
            : ''
          }
          {this.state.arrDeaths.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Deaths" chartData={this.state.arrDeaths} chartColors={this.state.deathsChartColors}></Chart>
            : ''
          }
          {this.state.isLoading ?
            <IonRow class="container" >
              <IonCol>
                <IonSpinner name="dots" />
              </IonCol>
            </IonRow>

            : ''}

        </IonContent>
      </IonPage>
    );
  }
};

export default withIonLifeCycle(Tab1);
