import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonRow, IonCol, IonSpinner, withIonLifeCycle, IonSelect, IonSelectOption } from '@ionic/react';
import './Tab2.css';

import Chart from '../components/Chart';
import moment from 'moment';
import axios from 'axios';

interface chartData {
  date: string,
  total: number
}

interface stateData {
  state: string,
  name: string,
  covid19Site: string
}

interface chartColors {
  borderColor: string,
  pointBorderColor: string,
  pointBackgroundColor: string,
  backgroundColor: string
}

interface MyState {
  arrPositive: chartData[],
  positiveChartColors: chartColors,
  arrHospitalized: chartData[],
  hospitalizedChartColors: chartColors,
  arrInIcu: chartData[],
  inIcuChartColors: chartColors,
  arrOnVentilators: chartData[],
  onVentilatorsChartColors: chartColors,
  arrRecovered: chartData[],
  recoveredChartColors: chartColors,
  arrDeaths: chartData[],
  deathsChartColors: chartColors,
  userStateName: '',
  userStateAbbr: '',
  isLoading: boolean,
  arrStates: stateData[]
}


class Tab2 extends React.Component<{}, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      arrPositive: [],
      positiveChartColors: {
        borderColor: "#077187",
        pointBorderColor: "#0E1428",
        pointBackgroundColor: "#AFD6AC",
        backgroundColor: "#74A57F"
      },
      arrHospitalized: [],
      hospitalizedChartColors: {
        borderColor: "#251F47",
        pointBorderColor: "#260F26",
        pointBackgroundColor: "#858EAB",
        backgroundColor: "#858EAB"
      },
      arrInIcu: [],
      inIcuChartColors: {
        borderColor: "#190B28",
        pointBorderColor: "#190B28",
        pointBackgroundColor: "#E55381",
        backgroundColor: "#E55381"
      },
      arrOnVentilators: [],
      onVentilatorsChartColors: {
        borderColor: "#784F41",
        pointBorderColor: "#784F41",
        pointBackgroundColor: "#BBE5ED",
        backgroundColor: "#BBE5ED"
      },
      arrRecovered: [],
      recoveredChartColors: {
        borderColor: "#4E5E66",
        pointBorderColor: "#4E5E66",
        pointBackgroundColor: "#31E981",
        backgroundColor: "#31E981"
      },
      arrDeaths: [],
      deathsChartColors: {
        borderColor: "#E06D06",
        pointBorderColor: "#E06D06",
        pointBackgroundColor: "#402A2C",
        backgroundColor: "#402A2C"
      },
      userStateName: "",
      userStateAbbr: "",
      isLoading: false,
      arrStates: []
    }
  }

  async ionViewWillEnter() {
    this.setState({ isLoading: true });
    this.setState({ arrStates: [] });

    const res = await axios.get('http://ip-api.com/json');
    this.setState({ userStateName: res.data.regionName })
    this.setState({ userStateAbbr: res.data.region })

    const stateRes = await axios.get("https://covidtracking.com/api/states/info");

    stateRes.data.forEach((stateData: any) => {
      const { state, name, covid19Site } = stateData;
      this.setState({ arrStates: [...this.state.arrStates, { state, name, covid19Site }] });
    })

    await this.getUserStateData(res.data.region);
    this.setState({ isLoading: false });
  }

  async getUserStateData(state: string) {
    const { data } =
      await axios.get(`https://covidtracking.com/api/states/daily`);

    let stateData = data.filter((s: any) => { return s.state === state });

    stateData.forEach((d: any) => {
      const date = moment(d.date, "YYYYMMDD").format("MM/DD");

      const {
        positive,
        hospitalizedCurrently,
        inIcuCurrently,
        onVentilatorCurrently,
        recovered,
        death
      } = d;

      if (positive)
        this.setState({ arrPositive: [...this.state.arrPositive, { date, total: positive }] });
      if (hospitalizedCurrently)
        this.setState({ arrHospitalized: [...this.state.arrHospitalized, { date, total: hospitalizedCurrently }] });

      if (inIcuCurrently)
        this.setState({ arrInIcu: [...this.state.arrInIcu, { date, total: inIcuCurrently }] });

      if (onVentilatorCurrently)
        this.setState({ arrOnVentilators: [...this.state.arrOnVentilators, { date, total: onVentilatorCurrently }] });

      if (recovered)
        this.setState({ arrRecovered: [...this.state.arrRecovered, { date, total: recovered }] });

      if (death)
        this.setState({ arrDeaths: [...this.state.arrDeaths, { date, total: death }] });
    });
  }

  async setUserState(stateAbbrv: any) {
    this.setState({ isLoading: true });
    this.setState({ userStateAbbr: stateAbbrv })

    this.setState({ arrPositive: [] });
    this.setState({ arrHospitalized: [] });
    this.setState({ arrOnVentilators: [] });
    this.setState({ arrInIcu: [] });
    this.setState({ arrRecovered: [] });
    this.setState({ arrDeaths: [] });

    let stateName = this.state.arrStates.filter((s: stateData) => {
      return s.state === stateAbbrv;
    })[0].name as any;

    this.setState({ userStateName: stateName });

    await this.getUserStateData(stateAbbrv);
    this.setState({ isLoading: false });
  }

  render() {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>{this.state.userStateName}</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">{this.state.userStateName}</IonTitle>
            </IonToolbar>
          </IonHeader>
          {!this.state.isLoading ? <IonSelect className="state-select-list" value={this.state.userStateAbbr} okText="Okay" cancelText="Dismiss" onIonChange={e => this.setUserState(e.detail.value)}>
            {this.state.arrStates.map((state: stateData) => {
              return (<IonSelectOption key={state.state} value={state.state}>{state.name}</IonSelectOption>)
            })}
          </IonSelect> : ''}
          {this.state.arrPositive.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Positive" chartData={this.state.arrPositive} chartColors={this.state.positiveChartColors}></Chart>
            : ''
          }
          {this.state.arrHospitalized.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Hospitalized" chartData={this.state.arrHospitalized} chartColors={this.state.hospitalizedChartColors}></Chart>
            : ''
          }
          {this.state.arrInIcu.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="In ICU" chartData={this.state.arrInIcu} chartColors={this.state.inIcuChartColors}></Chart>
            : ''
          }
          {this.state.arrOnVentilators.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="On Ventilators" chartData={this.state.arrOnVentilators} chartColors={this.state.onVentilatorsChartColors}></Chart>
            : ''
          }
          {this.state.arrRecovered.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Recovered" chartData={this.state.arrRecovered} chartColors={this.state.recoveredChartColors}></Chart>
            : ''
          }
          {this.state.arrDeaths.length > 0 && !this.state.isLoading ?
            <Chart chartTitle="Deaths" chartData={this.state.arrDeaths} chartColors={this.state.deathsChartColors}></Chart>
            : ''
          }
          {this.state.isLoading ?
            <IonRow class="container" >
              <IonCol>
                <IonSpinner name="dots" />
              </IonCol>
            </IonRow>

            : ''}

        </IonContent>
      </IonPage>
    );
  }
};

export default withIonLifeCycle(Tab2);
